### Safety

A collection of tools and resources for living in a post-paranoia world.

### Reading
* [How to encrypt your entire life in less than an hour][001] by Quincy Larson



### Tools

_If you wish to contribute, create an issue with links to further resources._

_Cheers, d._


[001]: https://medium.freecodecamp.com/tor-signal-and-beyond-a-law-abiding-citizens-guide-to-privacy-1a593f2104c3#.8s17j4efk "How to encrypt your entire life in less than an hour"